set fish_greeting
set fish_color_normal ffffff
set fish_color_command 61afef
set fish_color_quote e5c07b
set fish_color_redirection c678dd
set fish_color_end c678dd
set fish_color_error f2777a
set fish_color_param 98c379
set fish_color_selection white --bold --background=brblack
set fish_color_search_match bryellow --background=black
set fish_color_history_current --bold
set fish_color_operator 00a6b2
set fish_color_escape 00a6b2
set fish_color_cwd 98c379
set fish_color_cwd_root f2777a
set fish_color_valid_path --underline
set fish_color_autosuggestion d3dae3
set fish_color_user 98c379
set fish_color_host normal
set fish_color_cancel -r
set fish_pager_color_completion e5c07b
set fish_pager_color_description e5c07b
set fish_pager_color_prefix 66cccc --underline
set fish_pager_color_progress brwhite --background=cyan
set fish_color_comment 586e75
set fish_color_match --background=brblue

function n --description 'support nnn quit and change directory'
    # Block nesting of nnn in subshells
    if test -n NNNLVL
        if [ (expr $NNNLVL + 0) -ge 1 ]
            echo "nnn is already running"
            return
        end
    end

    # The default behaviour is to cd on quit (nnn checks if NNN_TMPFILE is set)
    # To cd on quit only on ^G, remove the "-x" as in:
    #    set NNN_TMPFILE "$XDG_CONFIG_HOME/nnn/.lastd"
    # NOTE: NNN_TMPFILE is fixed, should not be modified
    if test -n "$XDG_CONFIG_HOME"
        set NNN_TMPFILE "$XDG_CONFIG_HOME/nnn/.lastd"
    else
        set NNN_TMPFILE "$HOME/.config/nnn/.lastd"
    end

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    nnn -C $argv

    if test -e $NNN_TMPFILE
        source $NNN_TMPFILE
        rm $NNN_TMPFILE
    end
end

set -x NNN_COLORS '4215'

function __p_p
  function fish_prompt
    echo (pwd)"> "
  end
end
function p
  fish --private -C __p_p
end

function vterm_printf;
    if begin; [  -n "$TMUX" ]  ; and  string match -q -r "screen|tmux" "$TERM"; end
        # tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$argv"
    else if string match -q -- "screen*" "$TERM"
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$argv"
    else
        printf "\e]%s\e\\" "$argv"
    end
end

function vterm_prompt_end;
    vterm_printf '51;A'(whoami)'@'(hostname)':'(pwd)
end

functions --copy fish_prompt vterm_old_fish_prompt

function fish_prompt --description 'Write out the prompt; do not replace this. Instead, put this at end of your file.'
    # Remove the trailing newline from the original prompt. This is done
    # using the string builtin from fish, but to make sure any escape codes
    # are correctly interpreted, use %b for printf.
    printf "%b" (string join "\n" (vterm_old_fish_prompt))
    vterm_prompt_end
end
