Comfy Role
==========
A role to make the remote shell environment comfy.
Installs and configures fish and nnn among other tasks.

You can use it just like any other role in a playbook:
```yaml
- name: Install docker and caddy
  hosts: all
  roles:
    - role: comfy
    - role: docker
      become: yes
    - role: caddy
      become: yes
```

but make sure not to use `become` with this role or else some config
will be installed for the root user.

You can also run this role directly without an inventory file or playbook:
```bash
./run.yml -i user@12.34.56.78, -K
```
```bash
./run.yml -i user@localhost, -e "ansible_port=2222" -K
```
